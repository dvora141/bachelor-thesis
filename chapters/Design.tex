\chapter{Návrh}
Po dokončení analýzy je třeba provést návrh. Ten je důležitým procesem v~rámci softwarového vývoje. \cite{design-importance} Nejdříve je třeba zvolit vhodné nástroje a technologie, které nám usnadní jeho vytvoření. \ref{design:design-system-choice}, \ref{design:tool-choice} Poté je možné se pustit do samotného navrhování jednotlivých stránek. \ref{design:prototyping}

\section{Výběr návrhového systému} \label{design:design-system-choice}
Návrhový systém je kolekce opakovaně použitelných komponent, které jsou vytvořeny na základě jasných pravidel. Zkombinováním těchto komponent lze vytvořit libovolnou aplikaci. \cite{design-system}

Správný výběr návrhového systému velmi výrazně ovlivňuje konečný vzhled aplikace a její oblíbenost mezi uživateli. Přispívá k~zajištění konzistence a kontinuity v~rámci celého programu a definuje určitá pravidla a doporučení, kterých bychom se měli v~rámci celého produktu držet. To nám totiž zajistí takové chování, na které jsou uživatelé zvyklí. \cite{design-system}

Zároveň zpravidla existuje nějaká konkrétní implementace, která dodržuje všechny zásady a dodává předpřipravené komponenty, což usnadňuje a zrychluje vývoj. Nejznámějším návrhovým systémem \cite{alternatives-to-material-design} zcela jistě je \textsf{Material Design} \cite{material-design} od firmy Google, ale existují i~další alternativy, například \textsf{Flat Remix} \cite{flat-remix}, či \textsf{Grommet} \cite{grommet}.

\begin{samepage}
Při výběru jsem se zaměřil na tyto aspekty:
\begin{itemize}
 \item Množství implementací -- větší množství implementací znamená možnost využití mnoha FE frameworků.
 \item Podobnost s~ostatními firemními aplikacemi -- budoucí uživatelé tohoto systému totiž s~největší pravděpodobností již pracují se skladovým systémem Atlantis. Rád bych se co nejvíce přiblížil jeho vzhledu, aby se snadněji naučili provádět běžné činnosti v~novém systému.
 \item Osobní preference -- jak moc se mi líbí vzhled komponent daného návrhového systému.
\end{itemize}
\end{samepage}

\subsection{Material Design}
Material Design byl vyvinut jako nástupce Flat designu, řeší tedy některé jeho nedostatky. \cite{alternatives-to-material-design}

Výhody: 
\begin{itemize}
 \item Obsahuje komponenty, které se dobře hodí do moderního, digitálního světa. Stále si však udržuje některé analogové prvky.
 \item Standardy, které definuje, dodržuje velké množství webových stránek.
 \item Rozsáhlá dokumentace.
 \item Pro jeho popularitu existuje mnoho implementací, např. \textsf{Material UI} \cite{material-ui}, \textsf{Vuetify} \cite{vuetify} či \textsf{Angular Material}. \cite{angular-material}, \cite{material-design}
 \item Použit pro FE skladového systému Atlantis.
\end{itemize}

Nevýhody:
\begin{itemize}
 \item Někomu může vadit velká podobnost s~aplikacemi Googlu.
\end{itemize}

\subsection{Flat Remix}
Flat remix vychází z~již zmíněného Flat designu, ale poučil se z~jeho nedostatků. \cite{alternatives-to-material-design}

Výhody:
\begin{itemize}
 \item Jedná se v~podstatě jen o~CSS styly, není tedy třeba žádného JavaScript kódu. To znamená, že neexistuje žádná implementace, lze ho použít s~libovolným frameworkem.
 \item Jde o~již hotovou implementaci, máme k~dispozici komponenty, stačí je použít. \cite{flat-remix}
\end{itemize}

\begin{samepage}
Nevýhody:
\begin{itemize}
 \item Strohá dokumentace \cite{flat-remix}.
 \item Osobně se mi moc nelíbí vzhled komponent.
\end{itemize}
\end{samepage}

\subsection{Grommet}
Jako poslední bych zde rád zmínil Grommet. Jedná se z~části o~návrhový systém, z~části o~konkrétní implementaci. \cite{grommet}

Výhody:
\begin{itemize}
 \item Dodává své komponenty, které je velmi snadné použít.
 \item Klade velký důraz na použitelnost ze strany vývojářů. \cite{grommet}
 \item Na první pohled podobný se skladovým systémem Atlantis.
\end{itemize}

Nevýhody:
\begin{itemize}
 \item Vyvinut speciálně pro React. \cite{grommet}
\end{itemize}


\subsection{Shrnutí}
Dílčí hodnocení a celkový bodový zisk je v~tabulce \ref{tab:design-system-choice}. Rozhodl jsem se pro Material Design, který ze srovnání vyšel nejlépe.

\begin{table}\centering
    \caption{Výsledek průzkumu návrhových systémů}\label{tab:design-system-choice}
\begin{tabular}{|l|l|l|l|}
\hline
                                 & Material Design & Flat Remix & Grommet \\ \hline
Množství implementací            & 3               & 3          & 1       \\ \hline
Podobnost s firemními aplikacemi & 3               & 2          & 3       \\ \hline
Osobní preference                & 3               & 1          & 3       \\ \hline
Celkový bodový zisk              & 9               & 6          & 7       \\ \hline
\end{tabular}
\end{table}

\section{Výběr nástroje pro tvorbu wireframů} \label{design:tool-choice}
Nejdříve zpravidla dochází k~vytvoření wireframů. Ty mohou mít několik podob, ale ve své práci se omezím na základní wireframy, které budou sloužit jako Lo-Fi prototypy. Bude se tedy jednat o~grafické znázornění rozložení jednotlivých prvků na stránce. \cite{prototyping} 

Takto lze poměrně rychle vytvořit model a zkonzultovat ho s~vedoucím práce, který je zároveň budoucím uživatelem vznikajícího systému. Na to je třeba vybrat odpovídající nástroj. 

Jako příklad těch nejznámějších \cite{best-ui-tools} lze uvést \textsf{Balsamiq} \cite{balsamiq}, \textsf{Axure RP} \cite{axure-rp} nebo \textsf{Figma} \cite{figma}.

\begin{samepage}
Požadavky na nástroj:
\begin{itemize}
 \item Spolupráce ve více lidech -- zejména ze začátku budeme společně s~Janem analyzovat a navrhovat jednotlivé stránky, abychom co nejvíce sladili svůj styl. Zároveň tímto způsobem lze snadno sdílet návrh s~vedoucím, případně i~dalšími uživateli aktuálního systému, kteří budou validovat návrhy.
 \item Komponenty pro Material Design -- v~předchozí sekci \ref{design:design-system-choice} jsem se rozhodl pro použití Material Designu. Aby návrhy co nejvíce odpovídaly nakonec implementovanému výsledku, je vhodné použít tyto komponenty. Zároveň to urychlí proces navrhování.
 \item Dostupné ikony -- chceme je využít ve větší míře. Bez nich by návrhy nebyly úplné a hodně by se lišily od konečného výsledku.
\end{itemize}
\end{samepage}

\subsection{Balsamiq}
Jedná se o~nástroj zaměřený zejména na vytváření Lo-Fi prototypů. \cite{axure-rp}

Výhody:
\begin{itemize}
 \item Obsahuje velké množství UI prvků a templatů, které usnadňují tvorbu wireframů. Stačí si vybrat nějaký prvek z~menu a pouze ho přetáhnout na pracovní plochu, použití je tedy poměrně snadné.
 \item Export do PDF.
 \item Možnost spolupracovat na modelu v~týmu složeném z~více kolegů přes cloud.
 \item Webová verze -- umožňuje použití v~prohlížeči bez nutnosti instalace dalších programů.
 \item Třicetidenní verze zdarma. \cite{balsamiq}
\end{itemize}

Nevýhody:
\begin{itemize}
 \item Umožňuje pouze vytvářet odkazy na jiné wireframy -- nástroj není vhodný pro vytváření animací a interaktivních prvků. Je ale například možné dát odkaz na nějaké tlačítko, a tím učinit výsledný prototyp aspoň trochu interaktivní.
 \item Omezené množství ikon. \cite{balsamiq}
\end{itemize}

Ukázku uživatelského rozhraní programu lze vidět na obrázku \ref{fig:balsamiq}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/balsamiq}
    \caption{Uživatelské rozhraní programu Balsamiq}\label{fig:balsamiq}
\end{figure}


\subsection{Axure RP}
Velmi obsáhlý nástroj vhodný pro náročnější projekty, kde je třeba využít dynamická data. Vzhledem k~dostupným funkčnostem je to dobrá volba pro zkušené UI/UX návrháře. \cite{axure-rp}

Výhody:
\begin{itemize}
 \item Umožňuje vytvářet realistické a funkční prototypy, tedy Hi-Fi prototypy, které by již bylo možné použít pro testování s uživateli.
 \item Výborná podpora animací.
 \item Cloud úložiště.
 \item Export CSS a dokumentace.
 \item Existuje widget s~Material Design komponentami \cite{axure-material-design} a knihovna obsahující nejpoužívanější Material Design ikony \cite{axure-material-icons}.
 \item Dostupná časově omezená verze zdarma. \cite{axure-rp}
\end{itemize}

\begin{samepage}
Nevýhody:
\begin{itemize}
 \item Velmi obsáhlý program, který by bylo obtížnější se naučit používat.
 \item Spolupráce na modelu ve více lidech je možná pouze s~vyšší verzí licence. \cite{axure-rp}
\end{itemize}
\end{samepage}

Uživatelské rozhraní je vidět na obrázku \ref{fig:axure-rp}

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/axure-rp}
    \caption{Uživatelské rozhraní programu Axure RP}\label{fig:axure-rp}
\end{figure}

\subsection{Figma}
Figma nabízí mnohem více volnosti, je možné tvořit jak Lo-Fi, tak Hi-Fi prototypy. \cite{figma}

Výhody:
\begin{itemize}
 \item Lze ji využít k~tvorbě téměř jakéhokoli designu.
 \item V~rámci jednoho programu lze vytvořit více verzí a porovnat je mezi sebou.
 \item Možnost spolupracovat na modelu v~týmu složeném z~více kolegů přes cloud.
 \item Webová verze pro práci přímo v prohlížeči.
 \item Velké množství komunitou vytvořených rozšíření -- pluginů. Existuje již předpřipravená šablona s~Material Design komponentami \cite{figma-material-template} a rozšíření obsahující nejpoužívanější Material Design ikony \cite{figma-material-icons}.
 \item Verze zcela zdarma pro studijní účely. \cite{figma}
\end{itemize}

Vzhled webové aplikace je k~vidění na obrázku \ref{fig:figma}

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/figma}
    \caption{Uživatelské rozhraní programu Figma}\label{fig:figma}
\end{figure}

\subsection{Shrnutí}
Dílčí výsledky a celkový bodový zisk jsem zachytil v~tabulce \ref{tab:tool-choice}. Axure RP a Figma dosáhly téměř stejného bodového zisku. Nakonec jsem se rozhodl pro Figmu, neboť má verzi zdarma.

\begin{table}\centering
    \caption{Výsledek průzkumu nástrojů pro tvorbu wireframů}\label{tab:tool-choice}
\begin{tabular}{|l|l|l|l|}
\hline
                               & Balsamiq & Axure RP & Figma \\ \hline
Spolupráce ve více lidech      & 3        & 2        & 3     \\ \hline
Komponenty pro Material Design & 0        & 3        & 3     \\ \hline
Dostupné ikony                 & 1        & 3        & 3     \\ \hline
Celkový bodový zisk            & 4        & 8        & 9     \\ \hline
\end{tabular}
\end{table}

\section{Tvorba Lo-Fi prototypů} \label{design:prototyping}
Nejdůležitější částí této kapitoly je tvorba návrhu samostatných stránek, které jsem analyzoval v~předchozí kapitole. \ref{analysis:domain} Návrh probíhal iterativně, ke každé stránce jsem měl více konzultací se zadavatelem. Poznatky získané na těchto konzultací jsou zaznamenané přímo u~dané stránky.

\subsection{Výrobce}
Následující stránky jsme zpracovávali společně s~Janem, každý jsme připravili vlastní návrh. Ten jsme probrali mezi sebou a nakonec i~se zadavatelem. Výsledné návrhy jsme nakonec sloučili do jednoho.

\subsubsection{Přehled všech výrobců}\label{design:manufacturer-table}
Aktuální provedení tabulky mi přijde poměrně dobré, tedy hlavní změny budou v~grafické stránce -- oblečení do nového kabátu a přidání ikon. Následuje popis navrhovaných změn.
\begin{itemize}
 \item Sloupec \texttt{Řazení} bude zcela odstraněn. Záznamy v~tabulce budou nově řazeny podle tohoto údaje a zároveň bude možná i~rychlá úprava řazení pomocí přetažení řádků v~tabulce. 
 
 Toto řešení dá uživateli mnohem lepší představu, jak bude sekce výrobců zobrazená na e-shopu ve výsledku vypadat.
 \item Dojde k~přidání sloupce \texttt{Domény}, který bude obsahovat prvních pár domén daného výrobce (podle velikosti obrazovky). Ve spolupráci s~možností řazení podle tohoto sloupce bude velmi snadné zjistit, jací výrobci se na vybrané doméně nacházejí a jak jsou seřazeni.
 \item Do sloupce \texttt{Akce} bude přidána možnost výrobce smazat. Dále jednotlivé akce budou reprezentovány ikonami, které budou doplněny textovým popisem. Ten se objeví až po najetí kurzorem.
\end{itemize}

\subsubsection{Detail výrobce}\label{design:manufacturer-detail}
Protože se jedná o~první stránku, kterou jsme s~Janem navrhovali, po konzultaci s~vedoucím došlo k~většímu množství změn. Získané poznatky jsem však využil při návrhu kategorií a objednávek, jejich navržení tak bylo rychlejší a zadavatel byl s výsledkem spokojenější.

\subsubsection*{Karta Obecné}
Podle mého názoru formulář nevyužívá dostupné horizontální místo a proto je zbytečně dlouhý. Dále se mi nelíbí způsob zadávání některých polí. Proto jsme zde již při první schůzce s~Janem navrhli důležité změny.

\begin{itemize}
 \item \texttt{V doménách} by bylo mnohem lepší vytvořit tak, že uživatel bude vybírat z~nabídky dostupných domén, nebude tedy muset pokaždé psát název celé domény. 
 \item \texttt{Klíčová slova} by opět bylo vhodné vyplňovat výběrem z~již existující klíčových slov v systému.
 \item Jednotlivá pole budou mít přiřazenou ikonu, která bude mít stejný význam v~celém systému. To urychlí vyplňování, protože uživatel se díky ikonám snadněji zorientuje.
 \item \textit{Uložit} se bude nacházet až na konci stránky. Pokud tedy uživatel bude při vyplňování postupovat od shora dolů (což by měl), poté zadané informace snadno uloží.
 \item \textit{Storno} bude zcela odstraněno, neboť jeho funkčnost může být snadno simulována opuštěním stránky.
\end{itemize}

Zároveň náš vedoucí vyslovil požadavek na usnadnění zadávání položek, které jsou podobné, nebo spolu úzce souvisí. Například \texttt{SEO klíčové slovo} lze tedy předvyplnit z~\texttt{Názvu} nebo \texttt{Meta-popis} z~\texttt{Popisu}. 

Také by systém měl uživatele vyplňováním lépe provázet. Bylo by vhodné v~\texttt{Popisu} zvýrazňovat zadaná \texttt{Klíčová slova} a naopak jinak zvýrazňovat ty, která tam ještě obsažena nejsou. Další možností je upozornit na pole, která sice nejsou povinná, ale jejich vyplnění je vhodné. 

První verzi návrhu lze vidět na obrázku \ref{fig:manufacturer-detail-general-v1}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/manufacturer-detail-general-v1}
    \caption[{První verze návrhu karty \textsl{Obecné} v~detailu výrobce}]{První verze návrhu karty \textsl{Obecné} v~detailu výrobce}\label{fig:manufacturer-detail-general-v1}
\end{figure}

\subsubsection*{Karta Nastavení pro domény}
Na aktuálním řešení se mi zejména nelíbí, že uživatel musí znova vyplňovat doménu a jazyk pro každé pole, které chce nastavit. Následuje seznam navrhovaných změn.
\begin{itemize}
 \item Údaje již nebudou zobrazovány ve formě tabulky, neboť v~tabulce nejde stránkovat, vyhledávat ani filtrovat.
 \item Pole \texttt{Doména} a \texttt{Jazyk} bude nutné vyplnit pouze jednou a poté bude možné vyplnit hodnoty libovolného množství dostupných polí.
\end{itemize}
První verzi návrhu lze vidět na obrázku \ref{fig:manufacturer-detail-domain-v1}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/manufacturer-detail-domain-v1}
    \caption[{První verze návrhu karty \textsl{Nastavení pro domény} v~detailu výrobce}]{První verze návrhu karty \textsl{Nastavení pro domény} v~detailu výrobce}\label{fig:manufacturer-detail-domain-v1}
\end{figure}

Po předvedení první verze návrhu a diskuzi s~vedoucím byly provedeny tyto změny:
\begin{itemize}
 \item Odstranění pole \texttt{Řazení} v~detailu výrobce. Nastavení této funkcionality bude přesunuto do tabulky všech výrobců, využijeme přesouvání jednotlivých řádků tabulky. Velkou výhodou tohoto řešení bude, že uživatel ihned uvidí výsledek.
 \item \textsl{Obecné} bude nést název výchozí domény a bude sloužit pro nastavení výchozích údajů, tyto údaje zároveň budou použity ve výchozí doméně.
 \item Pevné sepětí jazyka s~doménou -- pro každou doménu bude pevně určený jazyk, neboť do budoucna se nepředpokládá podpora více jazyků na jedné doméně. Toto umožní zcela odstranit výběr jazyka, pouze bude zobrazena informace, v~jakém jazyce mají být údaje vyplněny.
 \item Karta \textsl{Nastavení pro domény} bude nahrazena kartami s~názvy jednotlivých domén daného výrobce. Umožní nastavit stejné údaje jako doposud. Dále bude graficky znázorňovat změny mezi hodnotami pro výchozí a aktuálně zvolenou doménu.
\end{itemize}
Druhou a poslední verzi návrhu lze vidět na obrázku \ref{fig:manufacturer-detail-v2}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/manufacturer-detail-v2}
    \caption[{Druhá verze návrhu detailu výrobce}]{Druhá verze návrhu detailu výrobce, nalevo je možné vidět hodnoty výchozí domény, které se liší od aktuálně zvolené domény}\label{fig:manufacturer-detail-v2}
\end{figure}

\subsection{Kategorie}

\subsubsection{Přehled všech kategorií}\label{design:category-table}
Stránka bude téměř shodná s přehledem všech výrobců, který je popsán v~sekci \ref{design:manufacturer-table}.

\subsubsection{Detail kategorie}
Rozdělení na karty zůstalo zachováno, pouze došlo ke sloučení funkcionality \textsl{Obecné} a \textsl{Nastavení pro domény} do jedné. Při navrhování jsem vycházel ze zkušeností a zpětné vazby, které jsem získal při návrhu detailu výrobce. 

V~návrhu mi vedoucím byly vytknuty pouze drobné nedostatky. U~všech karet se tlačítko \textit{Uložit} přesune nad nabídku karet a bude ukládat všechny karty. To se týká i~výrobce. \ref{design:manufacturer-detail} 

Případné změny, které se týkají pouze jedné z~karet, jsou detailně popsány dále.

\subsubsection*{Karta Obecné}
Karta bude velice podobná detailu výrobce \ref{design:manufacturer-detail}. Bude se lišit ve způsobu zobrazení výchozí domény, protože obrázky je možné nastavit zvlášť pro každou doménu.

\subsubsection*{Karta Parametry}\label{design:category-parameter}
Zde dojde zejména k~vizuálním změnám. Jednotlivé parametry už nebudou v~tabulce, ale jako jednotlivé formuláře. To nám umožní velice responzivní design -- na telefonu se jednotlivá pole budou nalézat pod sebou. 

Dále dojde k~ostranění tlačítka \textit{Přidat}. Systém bude totiž pole pro zadání nového parametru přidávat automaticky, jakmile uživatel vyplní předchozí parametr. Návrh této karty lze vidět na obrázku \ref{fig:category-detail-params}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/category-detail-params}
    \caption[{Návrh karty \textsl{Parametry} v~detailu kategorie}]{Návrh karty \textit{Parametry} v~detailu kategorie}\label{fig:category-detail-params}
\end{figure}

\subsubsection*{Karta Filtry}
Karta je podobná kartě parametrů \ref{design:category-parameter}, avšak filtry je třeba řadit a přiřazovat do skupin. Řazení bude prováděno přetahováním jednotlivých filtrů, na tuto možnost je uživatel upozorněn ikonou a změnou kurzoru po najetí na danou ikonu. Stejným způsobem je možné přesouvat filtry mezi jednotlivými skupinami a při přesunu jednoho filtru na druhý automaticky dojde k~vytvoření nové skupiny. 

Skupiny jsou také lépe rozpoznatelné, protože jejich položky jsou zleva odsazené. To je další z~důvodů, proč bylo třeba zbavit se zobrazení v~tabulce, v~té lze takového odsazení dosáhnout pouze obtížně. 

V~neposlední řadě došlo k~přesunu všech tlačítek nahoru, aby byla lépe dostupná. Vytvořený návrh lze vidět na obrázku \ref{fig:category-detail-filters}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/category-detail-filters}
    \caption[{Návrh karty \textsl{Filtry} v~detailu kategorie}]{Návrh karty \textsl{Filtry} v~detailu kategorie}\label{fig:category-detail-filters}
\end{figure}

\subsubsection*{Karta Bannery}
Bannery je třeba nastavit pro jednotlivé domény, avšak může se stát, že na některé doméně nechceme mít žádné bannery. Proto zde neexistuje žádná výchozí doména jako na kartě \textsl{Obecné}. Provedené změny:
\begin{itemize}
    \item Bannery již nebudou v~tabulce, bude se jednat o~vizuálně oddělené formuláře, jejichž uspořádání se bude přizpůsobovat šíři obrazovky. 
    \item Není třeba pro každý banner zvlášť nastavovat jazyk textů, protože jazyk je jednoznačně určen doménou a daný banner se v~nové verzi systému může vyskytovat pouze na doménách se stejným jazykem.
    \item Při umístění více aktivních bannerů na stejné místo na to bude uživatel upozorněn a formulář nebude možné uložit.
\end{itemize}
První verzi návrhu lze vidět na obrázku \ref{fig:category-detail-banners}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/category-detail-banners}
    \caption[{Návrh karty \textsl{Bannery} v~detailu kategorie}]{Návrh karty \textsl{Bannery} v~detailu kategorie}\label{fig:category-detail-banners}
\end{figure}

\begin{samepage}
Po konzultaci s~vedoucím byly provedeny drobné úpravy, které jsem zakomponoval až při implementaci.
\begin{itemize}
    \item Stránka je dělena na karty, pro každou doménu jedna. 
    \item Při zadání dvou a více domén je možné kopírovat bannery z~jiné domény, což je provedeno čistě na FE. To umožňuje uživateli poměrně snadno nastavit stejné bannery na více doménách, přičemž nejsou kladeny žádné další nároky na BE.
    \item Při volbě cíle jsou uživateli nabízeny existující produkty a kategorie, zároveň však může zadat libovolnou adresu.
\end{itemize}
\end{samepage}

\subsection{Objednávky}
Při návrhu jsem opět vyšel z~práce Bc. Iuliie Evseenko \cite{julia-thesis}, zejména jsem čerpal z~jejích nápadů a validoval je s~vedoucím. Poté jsem připravil vlastní návrh, neboť některé oblasti, zejména dobropisy, nebyly dostatečně propracované.

\subsubsection{Přehled všech objednávek}
Změny převzaté od Bc. Iuliie Evseenko:
\begin{itemize}
 \item Přidání nových sloupců -- \texttt{Zobrazeno}, \texttt{Komentář}, \texttt{Slovenská objednávka} a \texttt{Způsob platby}. Jejich detailní popis lze nalézt přímo v~její práci.
 \item Každý uživatel si bude moci nastavit, které sloupce se mu mají zobrazovat. \cite{julia-thesis}.
\end{itemize}

Osobně jsem ještě přidal nové akce:
\begin{itemize}
 \item \textit{Dobropisy} -- vede na speciální stránku. Ta bude podrobně popsána v~sekci \ref{design:order-credit-note}.
 \item \textit{Tisk faktury} -- umožňuje rychle vytisknout fakturu, pokud již byla vystavena.
\end{itemize}

Dále jsem využil barev pro urychlení orientace, jen v~menším měřítku než moje předchůdkyně. 

Rozhodl jsem se pro tyto tři barvy: \label{design:order-colors}
\begin{itemize}
 \item \textsf{Zelená} -- vše je v~pořádku. Například to znamená, že objednávka byla uhrazena, že je vše skladem nebo že zákazník udělil souhlas se zasláním dotazníku spokojenosti.
 \item \textsf{Oranžová} -- s~objednávkou by mohl být problém, například pokud zákazník vyplnil komentář, nebo se jedná o~slovenskou objednávku.
 \item \textsf{Červená} -- něco není v~pořádku, objednávka ještě nebyla zpracována nebo ji nelze zpracovat.
\end{itemize}

Návrh lze vidět na obrázku \ref{fig:order-table}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/order-table}
    \caption{Návrh přehledu všech objednávek}\label{fig:order-table}
\end{figure}


\subsubsection{Detail objednávky}
Změny převzaté od Bc. Iuliie Evseenko:
\begin{itemize}
 \item Propojení se skladovým systémem Atlantis. Ve své práci zmiňuje pouze v~sekci \textsl{Produkty}. Uživateli by měly být nabízeny dostupné produkty a jejich modely.
 
 Od vypracování návrhu Bc. Iuliie Evseenko však postoupil vývoj systému a ten nyní bude spravovat i~všechny zásilky a tisk jejich štítků. Správa zásilek se tedy výrazně zjednoduší a \textsl{Zásilky} budou jen přehledem informací získaných z~jiného systému.
 \item Zásilky různých dopravců budou evidovány v~jedné tabulce, tedy je třeba přidat sloupec \texttt{Dopravce}.
 \item \textsl{Komentář k~objednávce} již nebude tvořit samostatnou sekci, přesune se do \textsl{Podrobnosti objednávky}.
 \item Upozornění, pokud doručovací a fakturační adresa nejsou stejné. 
 \item Jednotlivé sekce budou zcela autonomní, uživatel bude moci měnit jejich pozici na stránce a jejich velikost. Systém si toto nastavení bude pamatovat.
 \item Odstranění faxu z~kontaktních údajů.
 \item Dobře viditelná tlačítka v~přehledu zásilek.
 \item Snadná změna pořadí produktů v~objednávce přesouváním pomocí myši.
 \item Jednotlivé historické události v~\textsl{Historie objednávky} budou ve výchozím stavu sbalené. Informace, zda se v~události nachází nějaký komentář, bude vidět ihned, ovšem pro zobrazení jeho znění je třeba danou událost rozbalit.
 \item \textsl{Aktualizovat objednávku} se přesune na konec \textsl{Historie objednávky}. \cite{julia-thesis}
\end{itemize}

\begin{samepage}
Mnou navržené úpravy:
\begin{itemize}
 \item \texttt{ID objednávky} se přesune do hlavičky \textsl{Podrobnosti objednávky} a některých dalších sekcí.
 \item \textsl{Kontaktní údaje} a \textsl{Adresy} se sloučí do jedné sekce \textsl{Údaje pro doručení objednávky}. 
 \item Zvýšil jsem počet informací, které budou vidět u~sbalených událostí v~\textsl{Historie objednávky}. Kromě stavu bude viditelné i~datum vzniku události a informace, zda událost byla vytvořena systémem nebo pracovníkem e-shopu.
 \item Do tabulky zásilek přibude akce pro sledování dané zásilky.
\end{itemize}
\end{samepage}

První verzi návrhu je možno vidět na obrázku \ref{fig:order-detail-v1}. Při konzultaci s~vedoucím byly domluveny tyto úpravy:
\begin{itemize}
 \item Přesun tlačítek nahoře -- \textit{DPH na Slovensko} se přesune do \textsl{Údaje pro doručení objednávky} a bude viditelné pouze v~případě, že bude zadáno platné slovenské DIČ. \textit{Faktura} (případně \textit{Vystavit fakturu}, pokud ještě nebyla vystavena) a \textit{Nastavit zaplaceno} se přesune do \textsl{Podrobnosti objednávky}.
 \item Informace, zda objednávka byla již zobrazena nějakým pracovníkem e-shopu, ikona oka, se přesune do hlavičky \textsl{Podrobnosti objednávky}.
 \item Dojde k~rozdělení \texttt{Zisk} na \texttt{Zisk za produkty} a \texttt{Celkový zisk}. Zároveň číslo nebude v~barevném rámečku, bude použit pouze barevný font.
 \item Některé sekce budou mít zleva barevnou čáru, ta bude vyjadřovat stav dané sekce. Význam barev je popsán v \ref{design:order-colors}.
 \item Pokud je fakturační a doručovací adresa stejná, dojde k~zobrazení pouze doručovací adresy. V~opačném případě budou u~fakturační adresy vyplněna pouze ta pole, která se liší. Zaměstnanec e-shopu tak velmi snadno zjistí přesné rozdíly mezi adresami.
 \item Do \textsl{Historie objednávky} přidat informaci, že zákazník nebyl informován. Možnost informovat zákazníka při vytvoření nové události by měla být implicitně aktivní.
 \item Způsob úhrady a dopravy je třeba měnit. To je třeba provádět dohromady, otevře se speciální modální okno, které je popsané v \ref{desing:order-delivery-and-payment}.
 \item Do~\textsl{Zásilky objednávky} je třeba přidat možnost vytvoření nové zásilky a dále sloupce se stavem zásilky a objednávky. Pokud ještě žádná zásilka neexistuje, systém by měl zobrazit informaci, že \uv{\textit{Je třeba zpracovat objednávku}}.
 \item Po kliknutí na telefon a e-mail vyvolat odpovídající akce -- nabídnout vytočení daného telefonního čísla či napsání e-mailu na danou adresu.
 \item Po kliknutí na adresu otevřít novou záložku v~prohlížeči s~vyhledáním této adresy ve webovém vyhledávači Seznam.cz.
 \item Pokud bude zadaná cena produktu v~\textsl{Produkty v objednávce} nižší než nákupní, je třeba uživatele informovat.
 \item Je třeba umožnit zrušení již vystavené faktury. Uživatel by zároveň měl být upozorněn na to, že by to neměl provést, pokud faktura již byla předána zákazníkovi.
\end{itemize}

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/order-detail-v1}
    \caption{První verze návrhu detailu objednávky}\label{fig:order-detail-v1}
\end{figure}

Rovněž jsem měl konzultaci se správkyní e-shopu. Ukazoval jsem jí vytvořený návrh a ptal se na akce, které nejčastěji provádí. Zde uvádím získané poznatky.
\begin{itemize}
 \item \textsl{Produkty v objednávce} by měly být ve výchozím stavu needitovatelné, aby uživatel omylem neprovedl změnu, kterou provést nechtěl. Přidal jsem tam tedy tlačítko \textit{Upravit}, jakmile je uživatel s~úpravami hotov, pomocí \textit{Uzamknout} opět dojde k~znemožnění editace.
 \item Nejčastějším scénářem je objednávka, u~které je vše v~pořádku a je možné ji expedovat. Uživatel musí nastavit stav \textsf{K expedici} a poté vystavit fakturu.
 
 Pro optimalizaci tohoto procesu by při nastavení stavu \textsf{K expedici} měl systém automaticky vystavit fakturu a nabídnout její tisk. Systém bude implicitně tisknout dvě faktury, jednu pro zákazníka a jednu pro e-shop. V~případě potřeby si však uživatel bude moci počet kopií nastavit. Tím se sníží počet kliknutí nutný ke zpracování objednávky.
 \item Pokud se čeká na uhrazení objednávky, pak je ve stavu \textsf{Čeká na uhrazení}. Jakmile dojde k~uhrazení a kontrole objednávky, změní uživatel stav na \textsf{K~expedici} a je tedy možné provést stejnou akci jako v~předchozím případě.
\end{itemize}

Poslední verze návrhu je na obrázku \ref{fig:order-detail-v2}.

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/order-detail-v2}
    \caption{Poslední verze návrhu detailu objednávky}\label{fig:order-detail-v2}
\end{figure}

\subsubsection{Dobropisy} \label{design:order-credit-note}
Tato stránka byla výrazněji přepracována, aby byly vyřešeny všechny podněty, které jsem získal od majitele e-shopu. \ref{analysis:order-credit-note-issues} Dále jsem přidal přehled všech dobropisů, které byly k~dané objednávce vystaveny, a všech již dobropisovaných produktů.

\begin{samepage}
Důležité změny, které by z~návrhu nemusely být patrné:
\begin{itemize}
 \item Při volbě způsobu dopravy a úhrady v~nově vznikající objednávce se otevře okno s~komponentou pro výběr dopravy a platby. \ref{desing:order-delivery-and-payment} V~celém systému budou tedy nabízeny stejné možnosti a jejich volba se bude provádět konzistentním způsobem.
 \item Při vystavování dobropisu je předvyplněn maximální možný počet kusů, který lze ještě dobropisovat. Tento počet lze upravovat, avšak systém uživateli dovolí zadat pouze platné hodnoty.
 \item Poslání dobropisu zákazníkovi je možné z~\textsl{Dobropisy objednávky}.
\end{itemize}
\end{samepage}

Návrh této stránky je na obrázku \ref{fig:order-credit-notes}

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/order-credit-notes}
    \caption{Návrh stránky pro správu dobropisů}\label{fig:order-credit-notes}
\end{figure}

\subsubsection{Výběr dopravy a platby} \label{desing:order-delivery-and-payment}
Dopravu a platbu je třeba volit dohromady, neboť pro různé typy dopravy jsou dostupné různé platby. Pro tento účel byla navržena samostatná komponenta. Její velkou výhodou je, že pracovník e-shopu vidí informace ve stejné podobě jako zákazník při objednání. 

Pokud zákazník zavolá, uživatel nového systému mu bude snadno a rychle schopen poskytnout informace o~dodací lhůtě a ceně dané dopravy. V~případě Balíku na poštu, Balíkovny nebo Zásilkovny navíc systém zobrazí otevírací dobu zvolené pobočky a její lokaci na mapě.

Návrh je k~vidění na obrázku \ref{fig:order-delivery-and-payment}

\begin{figure}\centering
    \includegraphics[width=\textwidth]{pictures/order-delivery-and-payment}
    \caption{Návrh komponenty pro výběr dopravy a platby}\label{fig:order-delivery-and-payment}
\end{figure}

\section{Závěr}
Vybral jsem Material Design jako návrhový systém, který použiji při návrhu i~při implementaci aplikace. Pro tvorbu wireframů jsem se rozhodl použít Figmu. Poté byly vytvořeny návrhy jednotlivých stránek, návrh měl typicky více iterací.

Společně s~Janem jsem navrhl výrobce. Při návrhu objednávek jsem vycházel z~práce Bc. Iuliie Evseenko \cite{julia-thesis}. Výsledný návrh objednávek jsem validoval se správkyní e-shopu. Dále byl probírán i~v~rámci týmu v~předmětu BI-SP1. \ref{realisation:team}

